package ar.edu.unlp.pas.ejercicio1.domain.service;

import ar.edu.unlp.pas.ejercicio1.domain.dto.PersonDto;
import ar.edu.unlp.pas.ejercicio1.domain.dto.mapper.PersonMapper;
import ar.edu.unlp.pas.ejercicio1.domain.model.Person;
import ar.edu.unlp.pas.ejercicio1.domain.model.Role;
import ar.edu.unlp.pas.ejercicio1.domain.repository.PersonRepository;
import ar.edu.unlp.pas.ejercicio1.domain.repository.RoleRepository;
import ar.edu.unlp.pas.ejercicio1.exception.ResourceAlreadyExist;
import ar.edu.unlp.pas.ejercicio1.exception.ResourceNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
@Transactional
@Slf4j
public class PersonService {
  private static final String PERSON = "Person";
  private final PersonRepository personRepository;
  private final RoleRepository roleRepository;
  private final PasswordEncoder passwordEncoder;

  public PersonDto getById(final Long id) {
    final Person person = personRepository.findPersonById(id).orElseThrow(() -> new ResourceNotFoundException(PERSON, "id", id));
    return PersonMapper.INSTANCE.personToPersonDto(person);
  }

  public List<PersonDto> getAll() {
    return personRepository.findAll().stream().map(PersonMapper.INSTANCE::personToPersonDto).collect(Collectors.toList());
  }

  public PersonDto registerNew(final Person person) {
    final boolean personIsPresent = personRepository.findPersonById(person.getId()).isPresent();
    if (personIsPresent) {
      throw new ResourceAlreadyExist(PERSON, "id", person.getId());
    }
    final Person personToSave = new Person(
        person.getName(),
        person.getLastName(),
        person.getDateOfBirth(),
        person.getEmail(),
        person.getInvoiceAddress(),
        person.getShippingAddress(),
        passwordEncoder.encode(person.getPassword()),
        new ArrayList<>(person.getRoles()));
    final Person savedPerson = personRepository.save(personToSave);
    return PersonMapper.INSTANCE.personToPersonDto(savedPerson);
  }

  public Person getByEmail(final String email) {
    return personRepository.findPersonByEmail(email).orElseThrow(() -> new ResourceNotFoundException(PERSON, "email", email));
  }

  public Role saveRole(final Role role) {
    return roleRepository.save(role);
  }
  public void addRoleToPerson(final String mail, final String roleName) {
    final var person = personRepository.findPersonByEmail(mail).orElseThrow();
    final var role = roleRepository.findByName(roleName).orElseThrow();
    person.getRoles().add(role);

  }

  public void delete(Long personId) {
    final Person person =
        personRepository.findPersonById(personId).orElseThrow(() -> new ResourceNotFoundException(PERSON, "id", personId));
    personRepository.deleteById(person.getId());
  }

  /**
   * If the person exists it updates the entity and persists it,
   * otherwise it creates it and persists it
   * @param person to be updated or created.
   * @return a {@link PersonDto} with updated entity values.
   */
  public PersonDto replacePerson(final Person person) {
    final Person savedPerson = personRepository.findPersonById(person.getId())
        .map(p -> {
          p.setName(person.getName());
          p.setLastName(person.getLastName());
          p.setDateOfBirth(person.getDateOfBirth());
          return personRepository.save(p);
        })
        .orElseGet(() -> personRepository.save(person));
    return PersonMapper.INSTANCE.personToPersonDto(savedPerson);
  }
}
