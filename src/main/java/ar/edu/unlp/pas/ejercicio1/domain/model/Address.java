package ar.edu.unlp.pas.ejercicio1.domain.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Address {

    public Address(String street, Integer number, String city, String province, String country) {
        this.street = street;
        this.number = number;
        this.city = city;
        this.province = province;
        this.country = country;
    }

    public Address(String street, Integer number, Integer floor, String apartment, String city, String province, String country) {
        this(street, number, city, province, country);
        this.floor = floor;
        this.apartment = apartment;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Street is mandatory")
    private String street;

    @NotNull(message = "Number is mandatory")
    @Positive(message = "Number must be positive")
    private Integer number;

    private Integer floor;

    private String apartment;

    @NotBlank(message = "City is mandatory")
    private String city;

    @NotBlank(message = "Province is mandatory")
    private String province;

    @NotBlank(message = "Country is mandatory")
    private String country;
}
