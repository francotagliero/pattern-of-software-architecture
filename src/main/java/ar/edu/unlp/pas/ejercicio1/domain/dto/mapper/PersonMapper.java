package ar.edu.unlp.pas.ejercicio1.domain.dto.mapper;

import java.time.LocalDate;
import java.time.Period;

import ar.edu.unlp.pas.ejercicio1.domain.dto.PersonDto;
import ar.edu.unlp.pas.ejercicio1.domain.model.Person;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PersonMapper {

  PersonMapper INSTANCE = Mappers.getMapper(PersonMapper.class);

  @Mapping(target = "fullName", expression = "java(person.getName() + ' ' + person.getLastName())")
  @Mapping(target = "age", source = "dateOfBirth", qualifiedByName = "dateOfBirthToAge")
  PersonDto personToPersonDto(Person person);

  @Named("dateOfBirthToAge")
  static Integer dateOfBirthToAge(final LocalDate dateOfBirth) {
    return Period.between(dateOfBirth, LocalDate.now()).getYears();
  }

}
