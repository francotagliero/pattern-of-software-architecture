package ar.edu.unlp.pas.ejercicio1.domain.repository;

import ar.edu.unlp.pas.ejercicio1.domain.model.Person;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

  Optional<Person> findPersonById(Long id);

  Optional<Person> findPersonByEmail(String email);
}
