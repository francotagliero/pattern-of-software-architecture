package ar.edu.unlp.pas.ejercicio1.domain.service;

import ar.edu.unlp.pas.ejercicio1.domain.model.Address;
import ar.edu.unlp.pas.ejercicio1.domain.model.Person;
import ar.edu.unlp.pas.ejercicio1.domain.model.Role;
import ar.edu.unlp.pas.ejercicio1.domain.repository.AddressRepository;
import ar.edu.unlp.pas.ejercicio1.domain.repository.PersonRepository;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class SeedService implements ApplicationRunner {
  @Autowired
  private final PersonRepository personRepository;
  @Autowired
  private final AddressRepository addressRepository;
  private final PasswordEncoder passwordEncoder;

  @Override
  public void run(ApplicationArguments args) throws Exception {
    Address ADDRESS_1 = new Address("9 de Julio", 323, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_2 = new Address("Dorrego", 123, 2, "D", "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_3 = new Address("Brown", 623, 1, "A", "Viedma", "Rio Negro", "Argentina");
    Address ADDRESS_4 = new Address("Volta", 546, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_5 = new Address("9 de Julio", 323, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_6 = new Address("Dorrego", 123, 2, "D", "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_7 = new Address("Brown", 623, 1, "A", "Viedma", "Rio Negro", "Argentina");
    Address ADDRESS_8 = new Address("Volta", 546, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_9 = new Address("9 de Julio", 323, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_10 = new Address("Dorrego", 123, 2, "D", "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_11 = new Address("Brown", 623, 1, "A", "Viedma", "Rio Negro", "Argentina");
    Address ADDRESS_12 = new Address("Volta", 546, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_13 = new Address("9 de Julio", 323, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_14 = new Address("Dorrego", 123, 2, "D", "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_15 = new Address("Brown", 623, 1, "A", "Viedma", "Rio Negro", "Argentina");
    Address ADDRESS_16 = new Address("Volta", 546, null, null, "Buenos Aires", "Buenos Aires", "Argentina");

    Role role1 = new Role("ROLE_USER");
    Role role2 = new Role("ROLE_ADMIN");

    final var password = passwordEncoder.encode("123456");
    personRepository.saveAll(List.of(
        new Person("Steven", "Robinson", LocalDate.of(1984, 4, 8), "stevenrobinson@email.com", ADDRESS_1,  Collections.singletonList(ADDRESS_2), password, List.of(role1)),
        new Person("Samuel", "Dodd", LocalDate.of(1979, 4, 11), "samueldodd@email.com", ADDRESS_3,  Collections.singletonList(ADDRESS_4), password, List.of(role2)),
        new Person("Sandra", "Cortez", LocalDate.of(1949, 9, 26), "sandracortez@email.com", ADDRESS_5, Collections.singletonList(ADDRESS_6), password, List.of(role1)),
        new Person("Hayden", "Carter", LocalDate.of(1957, 11, 24), "haydencarter@email.com", ADDRESS_7, Collections.singletonList(ADDRESS_8), password, List.of(role2)),
        new Person("Marc", "Nelson", LocalDate.of(1969, 7, 5), "marcnelson@email.com", ADDRESS_9, Collections.singletonList(ADDRESS_10), password, List.of(role2)),
        new Person("Rosemary", "Pedrick", LocalDate.of(1967, 3, 14), "rosemarypedric@email.com", ADDRESS_11, Collections.singletonList(ADDRESS_12), password, List.of(role1)),
        new Person( "Jerry", "Blake", LocalDate.of(1997, 8, 7), "jerryblack@email.com", ADDRESS_13, Collections.singletonList(ADDRESS_14), password, List.of(role2)),
        new Person("Mallory", "Phillips", LocalDate.of(1986, 4, 3), "malloryphillips@email.com", ADDRESS_15, Collections.singletonList(ADDRESS_16), password, List.of(role1))
    ));
  }
}
