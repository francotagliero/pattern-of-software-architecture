package ar.edu.unlp.pas.ejercicio1.domain.repository;

import ar.edu.unlp.pas.ejercicio1.domain.model.Role;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

  Optional<Role> findByName(String name);
}
