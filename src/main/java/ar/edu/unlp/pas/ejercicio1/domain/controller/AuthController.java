package ar.edu.unlp.pas.ejercicio1.domain.controller;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import ar.edu.unlp.pas.ejercicio1.domain.model.Person;
import ar.edu.unlp.pas.ejercicio1.domain.model.Role;
import ar.edu.unlp.pas.ejercicio1.domain.service.PersonService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1/token/refresh")
public class AuthController {
  PersonService personService;

  @GetMapping
  public void refreshToken(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    final String authorizationHeader = request.getHeader(AUTHORIZATION);
    if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
      try {
        final String refreshToken = authorizationHeader.substring("Bearer ".length());
        final Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
        final JWTVerifier verifier = JWT.require(algorithm).build();
        final DecodedJWT decodedJWT = verifier.verify(refreshToken);
        final String email = decodedJWT.getSubject();
        final Person user = personService.getByEmail(email);
        final String accessToken = JWT.create()
            .withSubject(user.getEmail())
            .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
            .withIssuer(request.getRequestURL().toString())
            .withClaim("roles", user.getRoles().stream().map(Role::getName).collect(Collectors.toList()))
            .sign(algorithm);
        final Map<String, String> tokens = Map.of(
            "access_token", accessToken,
            "refresh_token", refreshToken);
        response.setContentType(APPLICATION_JSON_VALUE);
        new ObjectMapper().writeValue(response.getOutputStream(), tokens);
      } catch (Exception exception) {
        log.error("Error logging in {}", exception.getMessage());
        response.setHeader("error", exception.getMessage());
        response.setStatus(FORBIDDEN.value());
        final Map<String, String> error = Map.of(
            "error_message", exception.getMessage()
        );
        response.setContentType(APPLICATION_JSON_VALUE);
        new ObjectMapper().writeValue(response.getOutputStream(), error);
      }
    } else {
      throw new RuntimeException("Refresh token is missing");
    }
  }
}
