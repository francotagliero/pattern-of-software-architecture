package ar.edu.unlp.pas.ejercicio1.domain.model.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class BirthValidator  implements ConstraintValidator<ValidateBirth, LocalDate> {

    private int min;
    private int max;

    @Override
    public void initialize(ValidateBirth constraintAnnotation) {
        this.min = constraintAnnotation.min();
        this.max = constraintAnnotation.max();
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        LocalDate minBirthDate = LocalDate.now().minusYears(max);
        LocalDate maxBirthDate = LocalDate.now().minusYears(min);
        return value.isBefore(maxBirthDate) && value.isAfter(minBirthDate);
    }

}