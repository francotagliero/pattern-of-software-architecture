package ar.edu.unlp.pas.ejercicio1.domain.dto;

import lombok.Value;

@Value
public class PersonDto {
  Long id;
  String fullName;
  int age;
}
