package ar.edu.unlp.pas.ejercicio1.filter;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@AllArgsConstructor
@Slf4j
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
  private final AuthenticationManager authenticationManager;
  /**
   * @param request  from which to extract parameters and perform the authentication
   * @param response the response, which may be needed if the implementation has to do a
   *                 redirect as part of a multi-stage authentication process (such as OpenID).
   * @return
   * @throws AuthenticationException
   */
  @Override
  public Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response) throws AuthenticationException {
    String email = request.getParameter("email");
    log.info("Username is: {}", email);
    String password = request.getParameter("password");
    log.info("Password is: {}", password);
    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(email, password);
    return authenticationManager.authenticate(authenticationToken);
  }

  /**
   * Default behaviour for successful authentication.
   * <ol>
   * <li>Sets the successful <tt>Authentication</tt> object on the
   * {@link SecurityContextHolder}</li>
   * <li>Informs the configured <tt>RememberMeServices</tt> of the successful login</li>
   * <li>Fires an {@link InteractiveAuthenticationSuccessEvent} via the configured
   * <tt>ApplicationEventPublisher</tt></li>
   * <li>Delegates additional behaviour to the
   * {@link AuthenticationSuccessHandler}.</li>
   * </ol>
   * <p>
   * Subclasses can override this method to continue the {@link FilterChain} after
   * successful authentication.
   *
   * @param request
   * @param response
   * @param chain
   * @param authentication the object returned from the <tt>attemptAuthentication</tt>
   *                   method.
   * @throws IOException
   * @throws ServletException
   */
  @Override
  protected void successfulAuthentication(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final FilterChain chain,
      final Authentication authentication
  ) throws IOException, ServletException {
    final User user = (User) authentication.getPrincipal();
    final Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
    final String accessToken = JWT.create()
        .withSubject(user.getUsername())
        .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
        .withIssuer(request.getRequestURL().toString())
        .withClaim("roles", user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
        .sign(algorithm);
    final String refreshToken = JWT.create()
        .withSubject(user.getUsername())
        .withExpiresAt(new Date(System.currentTimeMillis() + 30 * 60 * 1000))
        .withIssuer(request.getRequestURL().toString())
        .sign(algorithm);
    final Map<String, String> tokens = Map.of(
        "access_token", accessToken,
        "refresh_token", refreshToken);
    response.setContentType(APPLICATION_JSON_VALUE);
    new ObjectMapper().writeValue(response.getOutputStream(), tokens);
  }

  /**
   * Default behaviour for unsuccessful authentication.
   * <ol>
   * <li>Clears the {@link SecurityContextHolder}</li>
   * <li>Stores the exception in the session (if it exists or
   * <tt>allowSesssionCreation</tt> is set to <tt>true</tt>)</li>
   * <li>Informs the configured <tt>RememberMeServices</tt> of the failed login</li>
   * <li>Delegates additional behaviour to the
   * {@link AuthenticationFailureHandler}.</li>
   * </ol>
   *
   * @param request
   * @param response
   * @param failed
   */
  @Override
  protected void unsuccessfulAuthentication(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException failed)
      throws IOException, ServletException {
    super.unsuccessfulAuthentication(request, response, failed);
  }
}
