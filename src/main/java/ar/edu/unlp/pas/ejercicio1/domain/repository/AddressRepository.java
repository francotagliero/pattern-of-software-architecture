package ar.edu.unlp.pas.ejercicio1.domain.repository;

import ar.edu.unlp.pas.ejercicio1.domain.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}
