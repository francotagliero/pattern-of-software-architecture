package ar.edu.unlp.pas.ejercicio1.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

  public ResourceNotFoundException(final String resourceName, final String fieldName, final Object fieldValue) {
    super(String.format("Not found object type %s finding by %s using value: '%s'",
        resourceName, fieldName, fieldValue));
  }
}
