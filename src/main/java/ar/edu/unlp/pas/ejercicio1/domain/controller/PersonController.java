package ar.edu.unlp.pas.ejercicio1.domain.controller;

import java.util.List;

import ar.edu.unlp.pas.ejercicio1.domain.dto.PersonDto;
import ar.edu.unlp.pas.ejercicio1.domain.service.PersonService;
import ar.edu.unlp.pas.ejercicio1.domain.model.Person;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/person")
public class PersonController {

  private final PersonService personService;

  @GetMapping
  public ResponseEntity<List<PersonDto>> getAll() {
    return ResponseEntity.ok(
        personService.getAll()
    );
  }

  @GetMapping("{personId}")
  public ResponseEntity<PersonDto> getById(@PathVariable Long personId) {
    return ResponseEntity.ok(personService.getById(personId));
  }

  @PostMapping
  public ResponseEntity<PersonDto> registerNewPerson(@RequestBody final Person person) {
    return ResponseEntity.ok(personService.registerNew(person));
  }

  @DeleteMapping(path = "{personId}")
  public void deletePerson(@PathVariable("personId") Long personId) {
    personService.delete(personId);
  }

  @PutMapping
  public ResponseEntity<PersonDto> replacePerson(@RequestBody final Person person) {
    return ResponseEntity.ok(personService.replacePerson(person));
  }
}
