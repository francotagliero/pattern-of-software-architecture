package ar.edu.unlp.pas.ejercicio1.util;

import ar.edu.unlp.pas.ejercicio1.domain.dto.PersonDto;
import ar.edu.unlp.pas.ejercicio1.domain.model.Address;
import ar.edu.unlp.pas.ejercicio1.domain.model.Person;
import ar.edu.unlp.pas.ejercicio1.domain.model.Role;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import org.apache.commons.io.IOUtils;

public class TestUtils {
  public final static String PASSWORD_1 = "123456";
  public final static Role ROLE_1 = new Role("ROLE_ADMIN");
  public final static Role ROLE_2 = new Role("ROLE_USER");
  public final static Address ADDRESS_1 = new Address("9 de Julio", 323, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
  public final static Address ADDRESS_2 = new Address("Dorrego", 123, 2, "D", "Buenos Aires", "Buenos Aires", "Argentina");
  public final static Address ADDRESS_3 = new Address("Brown", 623, 1, "A", "Viedma", "Rio Negro", "Argentina");
  public final static Address ADDRESS_4 = new Address("Volta", 546, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
  public final static Person PERSON_1 = new Person("John", "Doe", LocalDate.of(1970, 3, 23), "johndoe@email.com", ADDRESS_1, Collections.singletonList(ADDRESS_2), PASSWORD_1, List.of(ROLE_1));
  public final static Person PERSON_2 = new Person("Jane", "Doe", LocalDate.of(1986, 7, 26),"janedoe@email.com", ADDRESS_3, Collections.singletonList(ADDRESS_4), PASSWORD_1, List.of(ROLE_2));
  public final static PersonDto PERSON_DTO_1 = new PersonDto(1L, "John Doe", 53);
  public final static PersonDto PERSON_DTO_2 = new PersonDto(2L, "Jane Doe", 36);

  /**
   * Load as String a resource located in the given resourceName
   *
   * @param resourceName location and/or name of resource
   * @return String with the file content
   * @throws IllegalStateException when file cannot be found
   */
  public static String getResourceAsString(final String resourceName) {
    final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    try (InputStream resource = classLoader.getResourceAsStream(resourceName)) {
      assert resource != null;
      return IOUtils.toString(resource, StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

}
