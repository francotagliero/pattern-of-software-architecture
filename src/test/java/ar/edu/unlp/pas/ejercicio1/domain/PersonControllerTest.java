package ar.edu.unlp.pas.ejercicio1.domain;

import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.PASSWORD_1;
import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.PERSON_DTO_1;
import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.PERSON_DTO_2;
import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.ROLE_2;
import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.getResourceAsString;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import ar.edu.unlp.pas.ejercicio1.domain.controller.PersonController;
import ar.edu.unlp.pas.ejercicio1.domain.dto.PersonDto;
import ar.edu.unlp.pas.ejercicio1.domain.model.Address;
import ar.edu.unlp.pas.ejercicio1.domain.model.Person;
import ar.edu.unlp.pas.ejercicio1.domain.service.PersonService;
import ar.edu.unlp.pas.ejercicio1.exception.ResourceNotFoundException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(controllers = PersonController.class)
class PersonControllerTest {

  private static final String PERSON_ENDPOINT = "/api/v1/person";

  @Autowired
  private MockMvc mvc;

  @MockBean
  PersonService personService;

  @Test
  void getAll() throws Exception {

    when(personService.getAll()).thenReturn(List.of(PERSON_DTO_1, PERSON_DTO_2));

    final var result = mvc.perform(
        MockMvcRequestBuilders.get(PERSON_ENDPOINT)
    );

    result.andExpect(status().isOk()).andExpect(content()
        .json(getResourceAsString("person/get_all_persons_response.json")));
  }

  @Test
  void getById() throws Exception {
    when(personService.getById(1L)).thenReturn(PERSON_DTO_1);
    final var result = mvc.perform(
        MockMvcRequestBuilders.get(Strings
            .concat(PERSON_ENDPOINT, "/1"))
    );
    result.andExpect(status().isOk());
  }

  @Test
  void getByIdNotFound() throws Exception {
    when(personService.getById(1L)).thenThrow(ResourceNotFoundException.class);
    final var result = mvc.perform(
        MockMvcRequestBuilders.get(Strings.concat(PERSON_ENDPOINT, "/1"))
    );
    result.andExpect(status().is4xxClientError());
  }

  @Test
  void replacePerson() throws Exception {
    final Address ADDRESS_1 = new Address("9 de Julio", 323, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    final Address ADDRESS_2 = new Address("Dorrego", 123, 2, "D", "Buenos Aires", "Buenos Aires", "Argentina");
    when(personService.replacePerson(new Person("Jan", "Doe", LocalDate.of(1993,3,23), "jandoe@email.com", ADDRESS_1, Collections.singletonList(ADDRESS_2), PASSWORD_1, List.of(ROLE_2))))
        .thenReturn(new PersonDto(3L, "Jan Doe", 30));
    final var result = mvc.perform(
        MockMvcRequestBuilders.put(PERSON_ENDPOINT)
            .content(getResourceAsString("person/request_person.json"))
            .contentType(APPLICATION_JSON));
    result.andExpect(status().isOk());
  }

}
