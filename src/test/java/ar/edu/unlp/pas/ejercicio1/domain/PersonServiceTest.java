package ar.edu.unlp.pas.ejercicio1.domain;

import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.PASSWORD_1;
import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.PERSON_1;
import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.PERSON_2;
import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.PERSON_DTO_1;
import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.PERSON_DTO_2;
import static ar.edu.unlp.pas.ejercicio1.util.TestUtils.ROLE_2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import ar.edu.unlp.pas.ejercicio1.domain.dto.PersonDto;
import ar.edu.unlp.pas.ejercicio1.domain.model.Address;
import ar.edu.unlp.pas.ejercicio1.domain.model.Person;
import ar.edu.unlp.pas.ejercicio1.domain.repository.PersonRepository;
import ar.edu.unlp.pas.ejercicio1.domain.service.PersonService;
import ar.edu.unlp.pas.ejercicio1.exception.ResourceAlreadyExist;
import ar.edu.unlp.pas.ejercicio1.exception.ResourceNotFoundException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PersonServiceTest {
  @Mock
  PersonRepository personRepository;

  @InjectMocks
  PersonService personService;

  @Test
  void getByIdWhenUsersNotFound() {
    when(personRepository.findPersonById(1L)).thenReturn(Optional.empty());
    assertThrows(ResourceNotFoundException.class, () -> personService.getById(1L));
  }

  @Test
  void getById() {
    when(personRepository.findPersonById(1L)).thenReturn(Optional.of(PERSON_1));
    final PersonDto response = personService.getById(1L);
    assertEquals(PERSON_DTO_1, response);
  }

  @Test
  void getAll() {
    when(personRepository.findAll()).thenReturn(List.of(PERSON_1, PERSON_2));
    final List<PersonDto> expected = List.of(PERSON_DTO_1, PERSON_DTO_2);
    final var response = personService.getAll();
    assertEquals(expected.size(), response.size());
    expected.forEach(
        personDTO -> assertTrue(response.contains(personDTO))
    );
  }

  @Test
  void registerNewWhenPersonAlreadyExist() {
    when(personRepository.findPersonById(1L)).thenReturn(Optional.of(PERSON_1));
    assertThrows(ResourceAlreadyExist.class, () -> personService.registerNew(PERSON_1));
  }

  @Test
  void registerNew() {
    when(personRepository.findPersonById(1L)).thenReturn(Optional.empty());
    when(personRepository.save(PERSON_1)).thenReturn(PERSON_1);
    final PersonDto response = personService.registerNew(PERSON_1);
    assertEquals(PERSON_DTO_1, response);
  }

  @Test
  void deleteWhenPersonNotExist() {
    when(personRepository.findPersonById(1L)).thenReturn(Optional.empty());
    assertThrows(ResourceNotFoundException.class, () -> personService.delete(1L));
  }
  @Test
  void replacePersonWhenIdExist() {
    final Address ADDRESS_1 = new Address("9 de Julio", 323, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    final Address ADDRESS_2 = new Address("Dorrego", 123, 2, "D", "Buenos Aires", "Buenos Aires", "Argentina");
    final Address ADDRESS_3 = new Address("Brown", 623, 1, "A", "Viedma", "Rio Negro", "Argentina");
    final Address ADDRESS_4 = new Address("Volta", 546, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    final Person newPerson = new Person("Jan", "Doe", LocalDate.of(1993,3,23), "jandoe@email.com", ADDRESS_1, Collections.singletonList(ADDRESS_2), PASSWORD_1, List.of(ROLE_2));
    final Person oldPerson = new Person("Jane", "Doe", LocalDate.of(1994,3,23), "jandoe@email.com", ADDRESS_3, Collections.singletonList(ADDRESS_4),  PASSWORD_1, List.of(ROLE_2));
    final PersonDto expectedDto = new PersonDto(3L, "Jan Doe", 30);
    when(personRepository.findPersonById(3L)).thenReturn(Optional.of(oldPerson));
    when(personRepository.save(newPerson)).thenReturn(newPerson);
    final PersonDto result = personService.replacePerson(newPerson);
    assertEquals(expectedDto, result);
  }
}
